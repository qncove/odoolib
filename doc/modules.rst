odoolib
=======

Accessing the API
+++++++++++++++++

Simple OpenERP/Odoo XML-RPC Access::

>>> from odoolib.odoolib import OdooLib
>>> ol = OdooLib('htp://oerp_xmlrpc_url:8069', 'db', 'user', 'passwd')

OdooLib Class
+++++++++++++

.. automodule:: odoolib

.. autoclass:: OdooLib
    :members:

External API access
+++++++++++++++++++

.. autofunction:: get_api
