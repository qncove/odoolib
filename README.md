# odoolib #

odoolib: very minimalistic, simple and easy to use XML-RPC Odoo/OpenERP library/wrapper

### What is it good for? ###

* Remote Odoo/OpenERP API access
* Remote development
* Automatic model detection: any model, basic or custom, is detected automatically
* Simple user login: no password/uid on each XML-RPC request

### How do I use it? ###

```
#!python

>>> from odoolib.api import OdooLib
>>> ol = OdooLib('http://oerp_xmlrpc_url:8069', 'db', 'user', 'passwd')
>>>

>>> # Get all records from any model
>>> ol.res_partner.all()
>>> ### ALL PARTNERS LIST ###

>>> # Get one record from a model
>>> ol.res_partner.get(id=1)

Out[5]: 
{'active': True,
 'additional_services': [],
 'bank_ids': [],
 'birthdate': False,
 'category_id': [],
 ...}

>>> # Add records to model
>>> ol.crm_lead.add({'name': 'New Lead with Dictionary'}) # Dictionary
12

>>> ol.crm_lead.add(name='New Lead with kwargs') # Kwargs
13

>>> # Delete a record in a model
>>> ol.crm_lead.delete(2)
True

>>> # Update record in a model
>>> ol.res_partner.update(1, {'name': 'My New Name with {}'}) # dict
True
>>> ol.res_partner.update(1, name='My New Name with Kwargs') # kwargs
True

```

### Roadmap 

- Workflow execution: developed but needs to be merged into this repo
- Internal docs (.rst files) are outdated
- Easy PDF report printing: function is already implemented but is not autodiscovered
- Better support for operators: something like object.less__than() or object.like()
- readthedocs integration: working on it right now

If you have any questions, just email me at [juan@qn.co.ve](mailto:juan@qn.co.ve) or follow me on twitter: [@vladjanicek](http://twitter.com/vladjanicek)

This library is licensed under the [GNU Affero General Public License](http://www.gnu.org/licenses/agpl-3.0.html)